package TimeSlot;
use strict;
use utf8;

# my $table = 'time_slots';

sub new
{
    my ($class, %opts) = @_;

    my $self = {};
    bless $self, $class;

    for my $opt (qw(wday h_start h_stop)) {
        $self->{ $opt } = $opts{ $opt } if $opts{ $opt };
    }

    return $self;
}

1;
