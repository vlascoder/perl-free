package Links::OrgPerson;
use strict;
use utf8;

my $table = 'org_person';

=sql
CREATE TABLE $table (
    id SERIAL NOT NULL,
    org_id UNSIGNED INTEGER NOT NULL,
    person_id UNSIGNED INTEGER NOT NULL,
    rel_type VARCHAR(30),
    position VARCHAR(100),
    UNIQUE (org_id, person_id, rel_type)
);
=cut

sub new
{
    my ($class, %opts) = @_;

    my $self = {};
    bless $self, $class;

    for my $opt (qw(org_id person_id type position)) {
        $self->{ $opt } = $opts{ $opt } if $opts{ $opt };
    }

    return $self;
}

1;
