package Links::OrgChief;
use strict;
use utf8;

use parent qw(Links::OrgPerson);

sub new
{
    my ($class, %opts) = @_;

    my $self = {};
    bless $self, $class;

    for my $opt (qw(org_id person_id position)) {
        $self->{ $opt } = $opts{ $opt } if $opts{ $opt };
    }
    $self->{'type'} = 'chief';

    return $self;
}

1;
