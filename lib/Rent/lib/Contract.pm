package Contract;
use strict;
use utf8;

my $table = 'contracts';

sub new
{
    my ($class, %opts) = @_;

    my $self = {};
    bless $self, $class;

    for my $opt (qw(number fee paid created expiration_date)) {
        $self->{ $opt } = $opts{ $opt } if $opts{ $opt };
    }

    return $self;
}

1;
