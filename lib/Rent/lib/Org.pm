package Org;
use strict;
use utf8;

my $table = 'orgs';

=sql
CREATE TABLE $table (
    id SERIAL NOT NULL,
    name VARCHAR(200),
    ogrn VARCHAR(30),
    inn VARCHAR(30),
    UNIQUE (ogrn),
    UNIQUE (inn)
);
=cut

sub new
{
    my ($class, %opts) = @_;

    my $self = {};
    bless $self, $class;

    for my $opt (qw(name ogrn inn)) {
        $self->{ $opt } = $opts{ $opt } if $opts{ $opt };
    }

    return $self;
}

1;
