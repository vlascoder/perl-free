package Comment;
use strict;
use utf8;

my $table = 'comments';

=sql
CREATE TABLE $table (
    id SERIAL NOT NULL,
    body TEXT,
    created TIMESTAMP WITHOUT TIME ZONE
);
=cut

sub new
{
    my ($class, %opts) = @_;

    my $self = {};
    bless $self, $class;

    for my $opt (qw(body)) {
        $self->{ $opt } = $opts{ $opt } if $opts{ $opt };
    }

    return $self;
}

1;
