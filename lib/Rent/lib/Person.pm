package Person;
use strict;
use utf8;

my $table = 'persons';

=sql
CREATE TABLE $table (
    id SERIAL NOT NULL,
    full_name VARCHAR(100),
    phone VARCHAR(100),
    email VARCHAR(100)
);
=cut

sub new
{
    my ($class, %opts) = @_;

    my $self = {};
    bless $self, $class;

    for my $opt (qw(full_name phone email)) {
        $self->{ $opt } = $opts{ $opt } if $opts{ $opt };
    }

    return $self;
}

1;
