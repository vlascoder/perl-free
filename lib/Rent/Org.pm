package Rent::Org;
use strict;
use utf8;

use parent 'Common';


sub _initClass
{
    my ($self) = @_;

    $self->{'_table'} = 'orgs';

    $self->{'_param_to_db_map'} = {
        'name'     => 'name',
        'inn'      => 'inn',
        'kpp'      => 'kpp',
        'ogrn'     => 'ogrn',
        'type'     => 'o_type',
        'address'  => 'address',
        'created'  => 'created',
        'modified' => 'modified'
    };

    $self->{'_core_params'} = [qw(inn name)];
}

1;
