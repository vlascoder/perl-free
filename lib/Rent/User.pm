package Rent::User;
use strict;
use utf8;

use parent 'Common';


sub _initClass
{
    my ($self) = @_;

    $self->{'_table'} = 'users';

    $self->{'_param_to_db_map'} = {
        'name'     => 'name',
        'phone'    => 'phone',
        'email'    => 'email',
        'created'  => 'created',
        'modified' => 'modified'
    };

    $self->{'_core_params'} = [qw(name)];
}

1;
