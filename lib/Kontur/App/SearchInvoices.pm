package Kontur::App::SearchInvoices;
use strict;
use warnings;
use utf8;
use feature 'state';

use parent 'Kontur::App';

use Kontur::Search::Query;


sub new
{
    my ($class, $req, $res) = @_;

    my $app = $class->SUPER::new();

    $app->{'request'}  = $req;
    $app->{'response'} = $res;

    $app->{'action_handler'} = {
        'register' => sub { $app->register(@_) },
        'status'   => sub { $app->status(@_) },
        'cancel'   => sub { $app->cancel(@_) }
    };

    return $app;
}

sub run
{
    my $app = shift;

    my $action = $app->{'request'}->param('action') // '';

    my $data = undef;
    if (exists $app->{'action_handler'}->{ $action }) {
        eval {
            $data = $app->{'action_handler'}->{ $action }->()
        };

        if ($@) {
            $data = {
                'status' => 'error',
                'error'  => $@
            };
        }
    }
    else {
        $data = {
            'status' => 'error',
            'error'  => 'Invalid action'
        };
    }

    $data //= {
        'status' => 'error',
        'error'  => 'Internal server error'
    };

    $app->{'response'}->body($app->{'json'}->pretty(1)->encode($data));

    $app->{'response'}->status(200)
        unless $app->{'response'}->status();

    $app->{'response'}->content_type('application/json')
        unless $app->{'response'}->content_type();
}

sub register
{
    my $app = shift;

    my @query_params = qw(owner_name owner_inn contractor_name contractor_inn period type date number);

    my $validation_status = $app->_validateParams(\@query_params);

    ($validation_status && $validation_status->{'values'})
    or push @{ $validation_status->{'errors'} }, "All params are empty: ".join(', ', @query_params);

    ($validation_status && @{ $validation_status->{'errors'} })
    and die join("\n", @{ $validation_status->{'errors'} }, '');

    my $query_str = $app->_makeJSONStrFromRequestParams(\@query_params);

    my $query = Kontur::Search::Query->new(
        (map { $_ => $app->{ $_ } } qw(debug logger dbh)),
        'Target'  => 'invoice',
        'Query'   => $query_str,
        'Status'  => 'registered',
        'Found'   => 0,
        'Size'    => 0,
        'Created' => $app->{'utils'}->datetime_hires(),
        'Updated' => $app->{'utils'}->datetime_hires()
    )
    or die "Cannot register search query\n";

    return {
        'id'     => $query->getParam('id'),
        'status' => $query->getParam('Status')
    };
}

sub status
{
    my $app = shift;

    my $id = $app->{'request'}->param('id')
        or die "Invalid query ID\n";

    my $query       = $app->_getQuery($id);
    my $result_file = $query->getParam('ResultFile');

    my %result = ();
    $result{'result_file'} = $result_file if $result_file;

    return {
        'id'     => $query->getParam('id'),
        'status' => $query->getParam('Status'),
        %result
    };
}

sub cancel
{
    my $app = shift;

    my $id = $app->{'request'}->param('id')
        or die "Invalid query ID\n";

    my $query  = $app->_getQuery($id);
    my $status = 'canceled';

    $query->setParams('Status' => $status)
        if $status ne $query->getParam('Status');

    return {
        'id'     => $id,
        'status' => $query->getParam('Status')
    };
}

sub _getQuery
{
    my $app = shift;
    my ($id) = @_;

    my $query = Kontur::Search::Query->new(
        (map { $_ => $app->{ $_ } } qw(debug logger dbh)),
        'id' => $id
    )
    or die "Search query not found (id=$id)\n";

    return $query;
}

sub _makeJSONStrFromRequestParams
{
    my $app = shift;
    my ($opts, $names, $params) = @_;

    my %data = ();
    for my $name (@$names) {
        $data{ $name } = $app->{'request'}->param($name);
    }

    return $app->{'json'}->encode({ %data });
}

sub _validateParams
{
    my $app = shift;
    my ($names) = @_;

    state $validation //= {
        'INN'    => sub { !$_[0] || $_[0] =~ /^(?:\d{10}|\d{12})$/ },
        'Period' => sub { !$_[0] || $_[0] =~ /^y20[01]\dq[1234]$/ },
        'Type'   => sub { !$_[0] || $_[0] =~ /^(?:8|9|10|11|12)$/ },
        'Date'   => sub { !$_[0] || $_[0] =~ /^\d\d\d\d-\d\d-\d\d$/ },
        'Number' => sub { !$_[0] || $_[0] =~ /^[\d\w]{1,1000}$/ },
        'Str'    => sub { !$_[0] || $_[0] =~ /\S/ }
    };

    state $param_type_map //= {
        'owner_inn'       => 'INN',
        'owner_name'      => 'Str',
        'contractor_inn'  => 'INN',
        'contractor_name' => 'Str',
        'period'          => 'Period',
        'type'            => 'Type',
        'date'            => 'Date',
        'number'          => 'Number'
    };

    my $status = {
        'errors' => [],
        'total'  => 0,
        'values' => 0
    };

    for my $name (@$names) {
        $status->{'total'}++;

        my $value = $app->{'request'}->param($name);
        $status->{'values'}++ if $value;

        my $valid = $validation->{ $param_type_map->{ $name } }->($value);

        push @{ $status->{'errors'} }, "Invalid $name" unless $valid;
    }

    return $status;
}

1;
