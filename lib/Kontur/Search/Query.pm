package Kontur::Search::Query;
use strict;
use warnings;
use utf8;

use parent 'Common';


sub _initClass
{
    my ($self) = @_;

    $self->{'_table'} = 'search_queries';

    $self->{'_param_to_db_map'} = {
        'Target'     => 'target',
        'Query'      => 'query',
        'Status'     => 'status',
        'Found'      => 'found',
        'Size'       => 'isize',
        'ResultFile' => 'result_file',
        'Created'    => 'created',
        'Updated'    => 'updated'
    };

    $self->{'_core_params'} = [qw(Target Query Status)];
}

sub new
{
    my $class = shift;
    my (%opts) = @_;

    for my $name (qw(logger dbh)) {
        die "Invalid $name" unless $opts{ $name };
    }

    my $self = $class->SUPER::new(%opts);

    return $self;
}

1;
