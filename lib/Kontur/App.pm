package Kontur::App;
use strict;
use warnings;
use utf8;

use DBI;
use JSON::PP;

use Logger;
use Utils;


sub new
{
    my ($class) = @_;

    my $debug = $ENV{'APP_DEBUG'};

    my $logger = Logger->new($ENV{'APP_LOG_FILENAME'})
        or die "Cannot init logger: $!";

    my $dbh = DBI->connect(
        sprintf("dbi:Pg:dbname=%s;host=%s", $ENV{"APP_DB_NAME"}, $ENV{"APP_DB_HOST"}),
        $ENV{"APP_DB_USER"},
        $ENV{"APP_DB_PASS"},
        {}
    )
    or die "Cannot connect to db: $!";

    my $json = JSON::PP->new();

    my $utils = Utils->new();

    my $app = {
        'debug'  => $debug,
        'logger' => $logger,
        'dbh'    => $dbh,
        'json'   => $json,
        'utils'  => $utils
    };

    bless $app, $class;

    return $app;
}

sub log
{
    my $app = shift;
    $app->{'logger'}->add(shift);
}

sub run
{
    my $app = shift;
    $app->log("Please redefine app's run subroutine");
}

1;
