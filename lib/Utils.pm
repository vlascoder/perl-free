package Utils;
use strict;
use warnings;
use utf8;

use Time::HiRes;


sub new
{
    my ($class, %opts) = @_;

    my $self = {};
    bless $self, $class;

    return $self;
}

sub datetime_hires
{
    my ($self, $time, $delta) = @_;

    $delta //= 0;
    $time  //= Time::HiRes::time() + $delta;

    my $msec = int(1000 * 1000 * ($time - int($time)));
    my ($SS, $MM, $HH, $dd, $mm, $yy) = localtime($time);

    my $str = join('T',
        sprintf('%04d-%02d-%02d', $yy + 1900, $mm + 1, $dd),
        sprintf('%02d:%02d:%02d', $HH, $MM, $SS)
    );

    $str .= sprintf('.%06d', $msec) if $msec > 0;

    return $str;
}

1;
