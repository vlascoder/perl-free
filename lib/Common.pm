package Common;
use strict;
use warnings;
use utf8;
use feature 'state';

use Utils;


sub new
{
    my ($class, %params) = @_;

    return if !%params
        || scalar(grep { !$params{ $_ } } qw(dbh logger));

    my $self = { %params };
    bless $self, $class;

    $self->_initClass();

    $self->{'_db_to_param_map'} = { reverse %{ $self->{'_param_to_db_map'} } };

    if (
        scalar($self->_checkParams(qw(id)))
        && scalar($self->_checkParams(@{ $self->{'_core_params'} }))
    ){
        $self->{'logger'}->add("Invalid id or ". join(',', @{ $self->{'_core_params'} }) ." params");
        return;
    }

    state $utils //= Utils->new();
    $self->{'_utils'} = $utils;

    $self->_init();
    if (!$self->{'id'}) {
        $self->{'logger'}->add("Cannot init $class");
        return;
    }

    return $self;
}

sub table
{
    my ($class) = @_;

    my $self = {};
    bless $self, $class;

    $self->_initClass();
    return $self->{'_table'};
}

sub _initClass
{
    my ($self) = @_;
    die "Override me";
}

sub getParam
{
    my ($self, $name) = @_;
    $self->{'logger'}->add("get $name: ".($self->{ $name } // 'undef')) if $self->{'debug'};
    return $self->{ $name };
}

sub setParams
{
    my ($self, %opts) = @_;

    if (exists $self->{'_param_to_db_map'}->{'modified'}) {
        $opts{'modified'} = $self->{'_utils'}->datetime_hires();
    }

    my @fields = ();
    my @values = ();
    my @actions = ();

    while (my ($name, $value) = each %opts) {
        $self->_setParamsWrapper($name, $value, \@fields, \@values, \@actions);
    }

    if (scalar @fields) {
        my $success = $self->{'dbh'}->do(
            "UPDATE ". $self->{'_table'} ."
            SET ". join(",", map { "$_ = ?" } @fields) ."
            WHERE id = ?",
            undef,
            @values, $self->{'id'}
        );

        map { $_->() } @actions if $success;
    }
}

sub _setParamsWrapper
{
    my ($self, $name, $value, $fields, $values, $actions) = @_;

    if (exists $self->{'_param_to_db_map'}->{ $name }) {
        push @$fields, $self->{'_param_to_db_map'}->{ $name };
        push @$values, $value;
        push @$actions, sub {
            $self->{ $name } = $value;
            $self->{'logger'}->add("set $name: ".$self->{ $name }) if $self->{'debug'};
        };
    }
}

sub _checkParams
{
    my ($self, @params) = @_;

    return grep { !$self->{ $_ } } @params;
}

sub _init
{
    my ($self) = @_;

    my @params      = sort keys %{ $self->{'_param_to_db_map'} };
    my @db_fields   = $self->_getDBFieldsByParams(@params);

    if ($self->{'id'}) {
        my (@data) = $self->{'dbh'}->selectrow_array(
            "SELECT ".join(',', @db_fields)." FROM ". $self->{'_table'} ." WHERE id = ?",
            undef,
            $self->{'id'}
        );

        if (scalar @data) {
            map {
                utf8::decode($data[0]) if 0 && $data[0];
                $self->{ $self->{'_db_to_param_map'}->{ $_ } } = shift @data
            } @db_fields;
        }
        else {
            undef $self->{'id'};
        }
    }

    if (!$self->{'id'}) {
        if (scalar(grep { !$self->{ $_ } } @{ $self->{'_core_params'} })) {
            $self->{'logger'}->add("Invalid core params");
            return;
        }

        my $now = $self->{'_utils'}->datetime_hires();
        for (my $i = 0; $i < scalar(@params); $i++) {
            if (scalar(grep { $params[ $i ] eq $_ } qw(created modified))) {
                $self->{ $params[ $i ] } = $now;
            }
        }

        $self->{'dbh'}->do(
            "INSERT INTO ". $self->{'_table'} ." (".join(',', @db_fields).") VALUES (".join(',', map { '?' } @db_fields).")",
            undef,
            (map { $self->{ $_ } } @params)
        );

        my $id = $self->{'dbh'}->last_insert_id(undef, undef, $self->{'_table'}, undef);
        if ($id) {
            $self->{'id'} = $id;
        }
    }

    return $self->{'id'};
}

sub _getDBFieldsByParams
{
    my ($self, @params) = @_;

    return map { $self->{'_param_to_db_map'}->{ $_ } } @params;
}

1;
