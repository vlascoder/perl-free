package RentApp;
use strict;
use warnings;
use utf8;

use DBI;
use Template;
use JSON::PP;

use Logger;
use Factory;
use Utils;


sub new
{
    my ($class, $req, $res) = @_;

    my $debug = $ENV{'APP_DEBUG'};

    my $logger = Logger->new($ENV{'APP_LOG_FILENAME'})
        or die "Cannot init logger: $!";

    my $dbh = DBI->connect(
        sprintf("dbi:mysql:dbname=%s;dbhost=%s", $ENV{"APP_DB_NAME"}, $ENV{"APP_DB_HOST"}),
        $ENV{"APP_DB_USER"},
        $ENV{"APP_DB_PASS"}, {
            'mysql_enable_utf8' => 1
        })
        or die "Cannot connect to db: $!";

    my $template = Template->new({
        'INCLUDE_PATH' => $ENV{'APP_TEMPLATE_DIR'}
        })
        or die "Cannot init templates: ". $Template::ERROR;

    my $json = JSON::PP->new();

    my $factory = Factory->new(
        'dbh'    => $dbh,
        'logger' => $logger,
        'debug'  => $debug
    );

    my $utils = Utils->new();

    my $app = {
        'request'   => $req,
        'response'  => $res,
        'debug'     => $debug,
        'logger'    => $logger,
        'dbh'       => $dbh,
        'template'  => $template,
        'json'      => $json,
        'factory'   => $factory,
        'utils'     => $utils
    };

    bless $app, $class;

    return $app;
}

sub log
{
    my $app = shift;
    $app->{'logger'}->add(shift);
}

sub run
{
    my $app = shift;

    if ($app->{'request'}->param('getenv')) {
        $app->{'response'}->body(_dump_env($app->{'request'}->env()));
    }
    else {
        my (undef, undef, $type, $action, @path) = split '/', $app->{'request'}->path_info();

        if (!$type || not grep { $type eq $_ } qw(org time)) {
            $app->{'response'}->status(302);
            $app->{'response'}->header('Location' => '/rent/time');

            my $body = qq{<meta http-equiv="refresh" content="0;/rent/time">};
            $app->{'response'}->body($body);
            $app->{'response'}->content_type('text/html');

            return;
        }

        $action //= 'default';

        my $vars;
        if ($type eq 'org') {
            $vars = $app->_processOrg($action, [@path]);
        }
        elsif ($type eq 'time') {
            $vars = { 'time' => $app->{'utils'}->datetime_hires() };
        }

        my $body;
        $app->{'template'}->process("$type/$action.html", $vars, \$body);

        $app->{'response'}->body($body);
    }

    $app->{'response'}->status(200)
        unless $app->{'response'}->status();

    $app->{'response'}->content_type('text/html')
        unless $app->{'response'}->content_type();
}

sub _processOrg
{
    my $app = shift;
    my ($action, $path) = @_;

    my %data = ();

    if ($action eq 'view') {
        my $id = shift @$path;
        my ($org) = $app->{'factory'}->load('Rent::Org', 'id' => $id);
        if ($org) {
            $data{'org'} = $org;
        }
        else {
            $data{'not_found'} = 1;
        }
    }

    return { %data };
}

sub _dump_env {
    my ($env) = @_;

    my @body = ();
    push @body, _env2html('ENV', {%ENV});
    push @body, _env2html('env', $env);

    return @body;
}

sub _env2html {
    my ($title, $env) = @_;
    return "<div>$title</div>", map { "<b>$_</b>: ".$env->{ $_ }."<br>" } sort keys %$env;
}

1;
