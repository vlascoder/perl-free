package Logger;
use strict;
use utf8;
use Utils;


sub new
{
    my ($class, $filename, $debug) = @_;

    return if !$filename;

    open LOG, ">>", $filename
        or die "Cannot open file: $!";
    close LOG;

    my $self = {
        'filename' => $filename,
        'debug'    => $debug
    };

    bless $self, $class;

    $self->{'utils'} = Utils->new();

    return $self;
}

sub add
{
    my ($self, $text, $params_ref) = @_;

    open LOG, ">>", $self->{'filename'}
        or die "Cannot open file: $!";

    binmode LOG, ":utf8";

    my $caller_sub = (caller(1))[3];

    my %params = %{ $params_ref || {} };
    #utf8::decode($text);

    say LOG '[', $self->date(), '][', $caller_sub, '] ',
        $params{'cdata'}
        ? $self->cdata($text)
        : $params{'sep'}
            ? $self->sep($text, $params{'sep'})
            : $text;

    close LOG;
}

sub cdata
{
    my ($self, $text) = @_;
    return join("\n", '<![CDATA[', $text, ']]>');
}

sub sep
{
    my ($self, $text, $sep) = @_;
    return join("\n", $sep, $text, $sep);
}

sub date
{
    my ($self) = @_;

    return $self->{'utils'}->datetime_hires();
}

1;
