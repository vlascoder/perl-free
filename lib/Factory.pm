package Factory;
use strict;
use utf8;
use Class::Load qw(load_class);


sub new
{
    my ($class, %params) = @_;

    return if !%params
        || scalar(grep { !$params{ $_ } } qw(dbh logger));

    my $self = { %params };
    bless $self, $class;

    return $self;
}

sub build
{
    my ($self, $class, %opts) = @_;

    load_class($class);

    return $class->new(
        (map { $_ => $self->{ $_ } } qw(dbh logger debug)),
        %opts
    );
}

sub load
{
    my ($self, $class, $field, $value) = @_;

    load_class($class);
    my $table = $class->table();

    my $sth = $self->{'dbh'}->prepare("SELECT id FROM $table WHERE $field = ?");
    $sth->execute($value);

    my @list = ();

    while (my ($id) = $sth->fetchrow_array()) {
        my $item = $class->new(
            (map { $_ => $self->{ $_ } } qw(dbh logger debug)),
            'id' => $id
        );
        push @list, $item if $item;
    }

    return @list;
}

1;
