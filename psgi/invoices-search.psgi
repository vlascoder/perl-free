#!/usr/bin/perl
use strict;
use warnings;
use utf8;

use Plack::Request;
use Plack::Response;

use lib './lib';
use Kontur::App::SearchInvoices;


my $app = sub {
    my $req = Plack::Request->new(shift);
    my $res = Plack::Response->new();

    my $_app = Kontur::App::SearchInvoices->new($req, $res);
    $_app->run();

    return $res->finalize();
};

