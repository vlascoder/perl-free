#!/usr/bin/perl

use strict;

my $Name = $ARGV[0];

if (!$Name || $Name !~ /\.txt$/)
{
    warn "Invalid input file";
    exit;
}

$Name =~ s/\.txt$//;

my @Lines = ();

open DATA, "${Name}.txt";

while (<DATA>)
{
    chomp($_);

    my ($Color, $xValue, $yValue, $Comment) = split(/;/, $_);

    if ($Color && ($xValue || $yValue))
    {
        my ($xOp, $yOp);

        $xOp = $1 if ($xValue && $xValue =~ s/^([\+\-])//);
        $yOp = $1 if ($yValue && $yValue =~ s/^([\+\-])//);

        push @Lines, {
            'Comments' => $Comment,
            'Color' => $Color,
            'x' => {
                'Op' => $xOp,
                'Value' => $xValue
            },
            'y' => {
                'Op' => $yOp,
                'Value' => $yValue
            },
        }
    }
}

my $Width = 1600;
my $Height = 900;

my $OffsetLeft = 30;
my $OffsetRight = 30;
my $OffsetTop = 60;
my $OffsetBottom = 30;

my $FrameWidth = $Width - $OffsetLeft - $OffsetRight;
my $FrameHeight = $Height - $OffsetTop - $OffsetBottom;

my $MaxWidth = 0;
my $MaxHeight = 0;

my %LastPoint = (
    'x' => 0,
    'y' => 0
);

my @LinesData = ();

for my $Line (@Lines)
{
    my $Data = {
        'color' => $Line->{'Color'},
        'x1' => $LastPoint{'x'},
        'y1' => $LastPoint{'y'},
    };

    for my $Param (qw(x y))
    {
        if (defined $Line->{ $Param }->{'Value'})
        {
            $LastPoint{ $Param } -= $Line->{ $Param }->{'Value'} if $Line->{ $Param }->{'Op'} eq '-';
            $LastPoint{ $Param } += $Line->{ $Param }->{'Value'} if $Line->{ $Param }->{'Op'} eq '+';
        }
    }

    $Data->{'x2'} = $LastPoint{'x'};
    $Data->{'y2'} = $LastPoint{'y'};

    push @LinesData, $Data;

    $MaxWidth = $LastPoint{'x'} if $LastPoint{'x'} > $MaxWidth;
    $MaxHeight = $LastPoint{'y'} if $LastPoint{'y'} > $MaxHeight;
}

my $kWidth = $MaxWidth / $FrameWidth;
my $kHeight = $MaxHeight / $FrameHeight;

my $k = $kWidth > $kHeight ? $kWidth : $kHeight;

my $MaxX = int($MaxWidth / $k) + $OffsetLeft + $OffsetRight;
my $MaxY = int($MaxHeight / $k) + $OffsetTop + $OffsetBottom;

my @Draw = ();

for my $Line (@LinesData)
{
    push @Draw, sprintf('-fill "%s" -draw "line %d,%d %d,%d"',
        $Line->{'color'},
        $OffsetLeft + int($Line->{'x1'} / $k),
        $OffsetTop + int($Line->{'y1'} / $k),
        $OffsetLeft + int($Line->{'x2'} / $k),
        $OffsetTop + int($Line->{'y2'} / $k),
    );
}

my $Cmd = "convert -size ${MaxX}x${MaxY} canvas:\"#fff\" ". join(' ', @Draw) ." ${Name}.png\n";
# print $Cmd, "\n";
system($Cmd);
