#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use feature 'say';
use threads;
use Thread::Queue;
use Time::HiRes qw(sleep);


my $q = Thread::Queue->new();

my $t = threads->create(sub{
    while (my $e = $q->dequeue()) {
        say "processing queue: $e";
    }
});

$q->enqueue(111);
$q->enqueue(qw(a b c));
sleep 2;
$q->enqueue(222);
$q->enqueue(undef);
$t->join();

if (0) {
my $t1 = threads->create(sub { sleep 2; say 'Hello from '. threads->tid() .'!'; threads->detach() if 0 });
my $t2 = threads->create(sub { sleep 4; say 'Hello from '. threads->tid() .'!'; threads->detach() if 0 });

$t2->join();
$t1->join();

if (0) {
    while (!$t1->is_detached() || !$t2->is_detached()) {
        say "Waiting...";
        sleep .1;
    }
}
}

say "done";
# $t->join();
