#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use feature 'say';

use POSIX;
use IO::Socket::INET;

my $server = IO::Socket::INET->new(
    'Proto'     => 'tcp',
    'LocalPort' => 9000,
    'Listen'    => SOMAXCONN,
    'Reuse'     => 1
) or die "Cannot init server";

my $parent = $$;
my @childs = ();

$SIG{'CHLD'} = 'IGNORE';
$SIG{'INT'} = sub {
    kill('KILL', @childs);
};

MAIN:
while (my $client = $server->accept()) {
    my $pid = fork;
    die "Cannot fork: $?" if not defined $pid;

    if ($pid) {
        push @childs, $pid;
        close $client;
    }
    else {
        $client->autoflush(1);

        CLIENT:
        while(my $in = <$client>) {
            $in =~ s/^\s+|\s+$//gs;
            say "$$> $in";
            say $client "$in ok";
            kill 'INT', $parent if $in eq 'stop';
        }
        exit;
    }
}

say "done";
