#!/usr/bin/perl
use strict;
use warnings;


map { warn "$_: " . test($_) } qw(Abba Abbat Abatment);

sub test
{
    my $text = shift;

    return unless $text && $text =~ /^[\w\s]+$/;

    $text =~ s/\s+//gs;

    my $result = 0;
    map { $result ^= 1 << (ord(lc $_) - 97) } split //, $text;

    return 1 >= scalar grep { $_ == 1 } split //, unpack "b*", pack "q", $result;
}

