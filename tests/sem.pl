use threads;
use Thread::Semaphore;
use Time::HiRes qw(sleep);

my $semaphore = Thread::Semaphore->new();
my $GlobalVariable :shared = 0;

$thr1 = threads->create(\&sample_sub);
$thr2 = threads->create(\&sample_sub);
$thr3 = threads->create(\&sample_sub);

sub sample_sub {
    my $SubNumber = threads->tid();
    my $TryCount = 10;
    my $LocalCopy;
    #sleep(1);
    while ($TryCount--) {
        $semaphore->down();
        $LocalCopy = $GlobalVariable;
        print("$TryCount tries left for sub $SubNumber "
             ."(\$GlobalVariable is $GlobalVariable)\n");
        # sleep rand .2;
        $LocalCopy++;
        $GlobalVariable = $LocalCopy;
        $semaphore->up();
        sleep rand 1;
    }
}

$thr1->join();
$thr2->join();
$thr3->join();
