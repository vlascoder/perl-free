#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use feature 'say';

use threads;
use threads::shared;

use IO::Socket::INET;

my $server = IO::Socket::INET->new(
    'Proto'     => 'tcp',
    'LocalPort' => 9000,
    'Listen'    => SOMAXCONN,
    'Reuse'     => 1
) or die "Cannot init server";

while (my $client = $server->accept()) {
    $client->autoflush(1);
    threads->create(sub {
        my $tid = threads->tid();
        while(my $in = <$client>) {
            $in =~ s/\s+$//s;
            say "$tid> $in";
            say $client "$in ok";
        }
        say "$tid> DETACHED";
        threads->detach();
    });
}

say "done";
