CREATE DATABASE IF NOT EXISTS rent CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
CREATE USER 'rent'@'localhost' IDENTIFIED BY '123';
GRANT ALL ON rent.* TO 'rent'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE orgs (
    id          INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    inn         VARCHAR(12),
    kpp         CHAR(9),
    ogrn        CHAR(13),
    name        VARCHAR(500),
    o_type      VARCHAR(10),
    address     TEXT,
    created     DATETIME,
    modified    DATETIME,
    UNIQUE      (inn),
    UNIQUE      (ogrn)
);

CREATE TABLE users (
    id          INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name        VARCHAR(500),
    phone       VARCHAR(100),
    email       VARCHAR(200),
    created     DATETIME,
    modified    DATETIME,
    UNIQUE      (name)
);

