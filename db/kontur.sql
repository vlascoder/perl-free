CREATE DATABASE kontur;
CREATE USER kontur WITH password '123';
GRANT ALL ON DATABASE kontur TO kontur;

CREATE TABLE organizations (
    id   bigserial NOT NULL,
    inn  varchar NOT NULL,
    name varchar,
    PRIMARY KEY (id)
);

CREATE TABLE invoices (
    period         varchar NOT NULL,
    owner_inn      varchar NOT NULL,
    itype          int NOT NULL,
    contractor_inn varchar NOT NULL,
    sdate          varchar NOT NULL,
    snumber        varchar NOT NULL,
    json           TEXT,
    PRIMARY KEY (period, owner_inn, itype, contractor_inn, sdate, snumber)
);

CREATE TABLE search_queries (
    id          serial NOT NULL,
    target      varchar NOT NULL,
    query       text NOT NULL,
    created     timestamp WITHOUT TIME ZONE NOT NULL,
    updated     timestamp WITHOUT TIME ZONE NOT NULL,
    status      varchar NOT NULL,
    found       int NOT NULL DEFAULT 0,
    isize       int NOT NULL DEFAULT 0,
    result_file text
    PRIMARY KEY (id)
);

