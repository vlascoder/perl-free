@ceils = (
    {
        'canvas' => {
            'type' => 'cold-stretch',
            'square' => 2000 * 3000
        },
        'rails' => [
            {
                'type' => 'harpoon',
                'length' => 2000,
                'wall' => [qw(granite concrete)],
                'boring-distance' => 200
            },
            {
                'type' => 'harpoon',
                'length' => (3 + 2) * 1000,
                'wall' => [qw(concrete)],
                'boring-distance' => 200
            },
            {
                'type' => 'harpoon',
                'length' => (3) * 1000,
                'wall' => [qw(drywall)],
                'boring-distance' => 150
            }
        ]
    }
);

1;
