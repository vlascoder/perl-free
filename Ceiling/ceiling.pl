#!/usr/bin/perl
use strict;
use utf8;

use Data::Dumper;


our (%actions, %mats, @ceils);
do './ceiling-data.pm';
do './ceils.pm';


for my $ceil (@ceils) {
    my %price = (
        'mat' => 0,
        'job' => 0
    );

    if (exists $mats{'canvas'}->{ $ceil->{'canvas'}->{'type'} }) {
        my $matCanvas = $mats{'canvas'}->{ $ceil->{'canvas'}->{'type'} };
        my $m2 = $ceil->{'canvas'}->{'square'} / 1000 / 1000;

        $price{'mat'} += $matCanvas->{'price'} * $m2;
        $price{'job'} += $actions{'stretching'}->{ $matCanvas->{'stretch-type'} } * $m2;
    }

    $price{'total'} = $price{'mat'} + $price{'job'};

    warn Dumper({ %price });
}
