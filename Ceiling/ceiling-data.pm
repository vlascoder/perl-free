%actions = (
    'boring' => {
        'concrete' => 10,
        'drywool' => 10,
        'wood' => 0,
        'rails' => 5,
        'ceramic' => 10,
        'granite' => 20
    },
    'boring-ratio' => {
        'small' => 1,
        'middle' => 5,
        'large' => 10
    },
    'mounting' => {
        'dowel' => 5,
        'screw' => 5
    },
    'stretching' => {
        'harpoon' => 100,
        'bead' => 120,
        'textile' => 100
    }
);

%mats = (
    'dowel' => {
        'concrete' => 1,
        'drywool' => 20,
        'wood' => 0
    },
    'screw' => {
        'concrete' => 2,
        'drywool' => 2,
        'wood' => 2
    },
    'rails' => {
        'harpoon' => {
            'metal' => {
                'length' => 2500,
                'price' => 30
            },
            'plastic' => {
                'length' => 2000,
                'price' => 20
            }
        },
        'kraab' => {
            'metal' => {
                'length' => 2000,
                'price' => 200
            },
        },
        'borderless' => {
            'metal' => {
                'length' => 2000,
                'price' => 125
            },
        }
    },
    'canvas' => {
        'cold-stretch' => {
            'stretch-type' => 'harpoon',
            'warm' => 0,
            'max-width' => 5000,
            'price' => 200
        },
        'hot-stretch' => {
            'stretch-type' => 'harpoon',
            'warm' => 1,
            'max-width' => 5000,
            'price' => 200
        },
        'descor' => {
            'stretch-type' => 'textile',
            'warm' => 0,
            'max-width' => 5000,
            'price' => 350
        }
    }
);

1;
