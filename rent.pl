#!/usr/bin/perl
use strict;
use utf8;
use DBI;
use JSON::PP;
use YAML;
$YAML::Indent = 4;

use lib './lib';
use Factory;
use Logger;


my $debug = 1;

my $logger = Logger->new("./log/rent.log");

my $dbh = DBI->connect("dbi:mysql:dbname=rent;dbhost=localhost", qw(rent 123), {
        'mysql_enable_utf8' => 1
    })
    or die "Cannot connect to db: $!";

my $json = JSON::PP->new();

my $factory = Factory->new(
    'dbh'    => $dbh,
    'logger' => $logger,
    'debug'  => $debug
);


# test_org();
test_user();


sub test_org
{
    my ($org) = $factory->load('Rent::Org',
        'inn' => 1234567890
    );

    $org //= $factory->build('Rent::Org',
        'inn' => 1234567890,
        'name' => 'Test',
        'ogrn' => 1234567890123
    );

    $org->setParams('address' => '121213 Кривоколенный переулок, д.3');
    # $logger->add(YAML::Dump($org));
}

sub test_user
{
    my ($user) = $factory->load('Rent::User',
        'name' => 'Vasya'
    );

    $user //= $factory->build('Rent::User',
        'name' => 'Vasya'
    );

    $user->setParams('phone' => time);
    # $logger->add(YAML::Dump($org));
}



# my $person = Person->new();
# use Data::Dumper;
# warn Dumper($person);
# 
# 
# my %person = (
#     'fill_name' => '',
#     'position' => '',
#     'phone' => ''
# );
# 
# my %org = (
#     'name' => '',
#     'orgn' => '',
#     'inn' => '',
#     'chief' => \%person
# );
# 
# my %timeSlot = (
#     'wday' => 0,
#     'h_start' => 0,
#     'h_stop' => 23
# );
# 
# my %comment = (
#     'body' => '',
#     'created' => '0000-00-00'
# );
# 
# my %contracts = (
#     'org' => \%org,
#     'number' => '',
#     'date_start' => '0000-00-00',
#     'date_stop' => '0000-00-00',
#     'time_slots' => [(\%timeSlot) x 0],
#     'fee' => 0,
#     'paid' => 0,
#     'comments' => [(\%comment) x 0]
# );
