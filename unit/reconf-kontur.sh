#!/bin/sh

# Remove items
#curl -X DELETE 127.0.0.1:8200/config/listeners/*:8300/pass
#curl -X DELETE 127.0.0.1:8200/config/routes
#curl -X DELETE 127.0.0.1:8200/config/applications/print-env

## curl -X DELETE 127.0.0.1:8200/config/routes/1
## curl -X DELETE 127.0.0.1:8200/config/applications/rent

# Load config
## curl -X PUT -d@unit/conf.json 127.0.0.1:8200/config

# Show config
## curl 127.0.0.1:8200/config


sudo curl --unix-socket /var/run/control.unit.sock \
    -X DELETE \
    http://localhost/config/routes/2

sudo curl --unix-socket /var/run/control.unit.sock \
    -X DELETE \
    http://localhost/config/applications/invoices-search

sudo curl --unix-socket /var/run/control.unit.sock \
    -X PUT \
    -d@unit/conf.json \
    http://localhost/config

sudo curl --unix-socket /var/run/control.unit.sock \
    http://localhost/config
