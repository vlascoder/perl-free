#!/usr/bin/perl
use strict;
use warnings;
use utf8;

my $app = sub {
    my $env = shift;
    return [
        '200',
        [ 'Content-Type' => 'text/plain' ],
        [ print_env($env) ]
    ];
};

sub print_env
{
    my ($env) = @_;
    return join("\n", map { join(": ", $_, $env->{ $_ }) } sort keys %$env);
}
