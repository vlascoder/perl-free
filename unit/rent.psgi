#!/usr/bin/perl
use strict;
use warnings;
use utf8;

use Plack::Request;
use Plack::Response;

use lib './lib';
use RentApp;


my $app = sub {
    my $req = Plack::Request->new(shift);
    my $res = Plack::Response->new();

    my $rent_app = RentApp->new($req, $res);
    $rent_app->run();

    return $res->finalize();
};

