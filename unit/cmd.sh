# Setup
sudo systemctl enable unit
sudo systemctl restart unit
sudo service unit stop
sudo service unit start
sudo unitd --control 127.0.0.1:8200

# Show config
curl 127.0.0.1:8200
curl 127.0.0.1:8200/config
curl 127.0.0.1:8200/config/[listeners|routes|applications]

# Remove config items
curl -X DELETE 127.0.0.1:8200/config/listeners/*:8300
curl -X DELETE 127.0.0.1:8200/config/applications/print-env

# Load config
curl -X PUT -d@unit/conf.json 127.0.0.1:8200/config

